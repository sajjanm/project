package com.sajjanm.service.impl;

import com.sajjanm.constant.StatusConstants;
import com.sajjanm.dao.ProductDAO;
import com.sajjanm.entity.Product;
import com.sajjanm.service.ExcelService;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Sajjan Mishra
 */
public class ExcelServiceImpl implements ExcelService {

    @Inject
    private ProductDAO productDAO;

    @Override
    public String test() {
        return "I am working";
    }

    @Override
    public Response convertFromExcel(InputStream inputStream) {

        List<Product> productLists = new ArrayList<>();
        Row row;
        try {
            Workbook workbook = WorkbookFactory.create(inputStream);

            Sheet sheet = null;
            for (Sheet s : workbook) {
                if (s.getSheetName().equalsIgnoreCase("Product_Sample")) {
                    sheet = s;
                    System.out.println("Requested product file sheet was found!");
                }
            }
            if (sheet == null) {
                return Response.status(200).entity("The title of sheet was not matched").build();
            }

            Iterator< Row> rowIterator = sheet.iterator();
            Map<String, Integer> map = new HashMap<>();

            if (rowIterator.hasNext()) {
                row = rowIterator.next();

                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cellTitle = cellIterator.next();
                    switch (cellTitle.getStringCellValue()) {
                        case "Product Description":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Vendor Name":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Long Description":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Customer Price":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Retail Price":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Unit":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Availability Flag":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "Available Quantity":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                        case "SKU":
                            map.put(cellTitle.getStringCellValue(), cellTitle.getColumnIndex());
                            break;
                    }
                }
            }
            while (rowIterator.hasNext()) {
                Product newProduct = new Product();
                row = rowIterator.next();
                Iterator< Cell> cellIterator = row.cellIterator();
                DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();

                    for (Map.Entry<String, Integer> entry : map.entrySet()) {
                        if (cell.getColumnIndex() == entry.getValue()) {
                            setValueForClass(newProduct, entry.getKey(), (String) formatter.formatCellValue(cell));
                        }
                    }
                }
                newProduct.setBatch(new Date());
                newProduct.setStatus(StatusConstants.ACTIVE.getName());
                productLists.add(newProduct);
            }

        } catch (Exception e) {
        }
        if (save(productLists) == 0) {
            return Response.status(201).entity(productLists.size() + " number of items were inserted/ updated successfully").build();
        } else {
            return Response.status(200).entity("Something went wrong. Please try again later!").build();

        }

//        return Response.status(200).entity("The title of sheet was matched but no further code at the moment").build();
    }

    private void setValueForClass(Product product, String fieldName, Object fieldValue) {
        switch (fieldName) {
            case "Product Description":
                product.setProductDescription((String) fieldValue);
                break;
            case "Vendor Name":
                product.setVendorName((String) fieldValue);
                break;
            case "Long Description":
                product.setLongDescription((String) fieldValue);
                break;
            case "Customer Price":
                product.setCustomerPrice((String) fieldValue);
                break;
            case "Retail Price":
                product.setRetailPrice((String) fieldValue);
                break;
            case "Unit":
                product.setUnit((String) fieldValue);
                break;
            case "Availability Flag":
                product.setAvailabilityFlag((String) fieldValue);
                break;
            case "Available Quantity":
                product.setAvailabilityQuantity((String) fieldValue);
                break;
            case "SKU":
                product.setAvailabilityQuantity((String) fieldValue);
                break;
        }
    }

    private int save(List<Product> products) {
        try {
            for (Product p : products) {
                Product tempProduct = null;
                tempProduct = productDAO.getBySku(p.getSku());
                if (tempProduct == null) {
                    productDAO.save(p);
                } else {
                    tempProduct.setAvailabilityQuantity(tempProduct.getAvailabilityQuantity() + p.getAvailabilityQuantity());
                    productDAO.update(tempProduct);
                }
            }
            System.out.println("Updating Excel File Completed! ");
            return 0;
        } catch (Exception e) {
            System.out.println("Error Saving Excel File Details To Database");
            e.printStackTrace();
            return 1;
        }
    }

    @Override
    public Response manageSearch(String description, String vendorName, String customerPrice) {
//        public List<Product> manageSearch(String description, String vendorName, String customerPrice)

        return Response.status(200).entity(productDAO.manageSearch(description, vendorName, customerPrice)).build();
    }

    @Override
    public Response getProductById(Long id) {

        return Response.status(200).entity(productDAO.getById(id)).build();
    }

}
