/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.service.impl;

import com.sajjanm.dao.AdminDAO;
import com.sajjanm.entity.Admin;
import com.sajjanm.request.AdminRequest;
import com.sajjanm.service.AdminService;
import java.util.Date;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

/**
 *
 * @author Sajjan
 */
public class AdminServiceImpl implements AdminService {

    @Inject
    AdminDAO adminDAO;

    @Override
    public Response verifyLogin(AdminRequest adminRequest) {
        if (adminDAO.verifyPassword(adminRequest) == Boolean.TRUE) {
            return Response.status(200).entity("Login in successful").build();
        } else {
            return Response.status(401).entity("Username or Password is incorrect. Please try again later. ").build();
        }
    }

    @Override
    public Response createAdmin(AdminRequest adminRequest) {
        
        Admin admin = new Admin();
        admin.setCreatedDate(new Date());
        admin.setEmail(adminRequest.getEmail());
        admin.setFirstName(adminRequest.getFirstName());
        admin.setLastName(adminRequest.getLastName());
        admin.setLocation(adminRequest.getLocation());
        admin.setPassword(adminRequest.getPassword());
        admin.setUserName(adminRequest.getUserName());

        if (adminDAO.save(admin) == Boolean.TRUE) {
            return Response.status(201).entity("admin created successfully").build();
        } else {
                return Response.status(400).entity("admin could not be created").build();
        }
    }

}
