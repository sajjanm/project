/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.service;

import com.sajjanm.request.AdminRequest;
import javax.ws.rs.core.Response;

/**
 *
 * @author Sajjan
 */
public interface AdminService {
    
    public Response verifyLogin(AdminRequest adminRequest);
    
    public Response createAdmin (AdminRequest adminRequest);
}
