/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.service;

import com.sajjanm.entity.Product;
import java.io.InputStream;
import java.util.List;
import javax.ws.rs.core.Response;

/**
 *
 * @author Sajjan Mishra
 */
public interface ExcelService {

    public String test();

    public Response convertFromExcel(InputStream inputStream);
    
    public Response getProductById(Long id);
    
    public Response manageSearch(String description, String vendorName, String customerPrice);

}
