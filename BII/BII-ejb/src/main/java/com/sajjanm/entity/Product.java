/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Sajjan Mishra
 */
@Entity
@Table
public class Product implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PRODUCT_DESCRIPTION", unique = true)
    private String productDescription;

    @Column(name = "VENDOR_NAME")
    private String vendorName;

    @Column(name = "CUSTOMER_PRICE")
    private String customerPrice;

    @Column(name = "RETAIL_PRICE")
    private String retailPrice;

    @Column(name = "AVAILABILITY_FLAG")
    private String availabilityFlag;

    @Column(name = "AVAILABILITY_QUANTITY")
    private String availabilityQuantity;

    @Column(name = "BATCH")
    @Temporal(TemporalType.TIMESTAMP)
    private Date batch;

    @Column(name = "LONG_DESCRIPTION")
    private String longDescription;

    @Column(name = "UNIT")
    private String unit;

    @Column(name = "SKU")
    private String sku;

    @Column(name = "STATUS")
    private String status;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(String customerPrice) {
        this.customerPrice = customerPrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getAvailabilityFlag() {
        return availabilityFlag;
    }

    public void setAvailabilityFlag(String availabilityFlag) {
        this.availabilityFlag = availabilityFlag;
    }

    public String getAvailabilityQuantity() {
        return availabilityQuantity;
    }

    public void setAvailabilityQuantity(String availabilityQuantity) {
        this.availabilityQuantity = availabilityQuantity;
    }

    public Date getBatch() {
        return batch;
    }

    public void setBatch(Date batch) {
        this.batch = batch;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
