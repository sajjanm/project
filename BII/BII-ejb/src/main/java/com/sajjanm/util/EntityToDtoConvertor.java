/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.util;

import com.sajjanm.dto.ProductDto;
import com.sajjanm.entity.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sajjan Mishra
 */
public class EntityToDtoConvertor {
    
    public ProductDto convertProductToProductDto(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setAvailabilityFlag(product.getAvailabilityFlag());
        productDto.setAvailabilityQuantity(product.getAvailabilityQuantity());
        productDto.setBatch(product.getBatch());
        productDto.setCustomerPrice(product.getCustomerPrice());
        productDto.setId(product.getId());
        productDto.setLongDescription(product.getLongDescription());
        productDto.setProductDescription(product.getProductDescription());
        productDto.setRetailPrice(product.getRetailPrice());
        productDto.setSku(product.getSku());
        productDto.setStatus(product.getStatus());
        productDto.setUnit(product.getUnit());
        productDto.setVendorName(product.getVendorName());
        
        return productDto;
    }
    
    public List<ProductDto> convertProductListToProductDtoList(List<Product> productList){
        List<ProductDto> productDtolist = new ArrayList<>();
        
        productList.stream().forEach((product) -> {
            productDtolist.add(convertProductToProductDto(product));
        });
        return productDtolist;
    }
    
}
