/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.dto;

import java.util.Date;

/**
 *
 * @author Sajjan Mishra
 */
public class ProductDto {

    private Long id;
    private String productDescription;
    private String vendorName;
    private String customerPrice;
    private String retailPrice;
    private String availabilityFlag;
    private String availabilityQuantity;
    private Date batch;
    private String longDescription;
    private String unit;
    private String sku;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(String customerPrice) {
        this.customerPrice = customerPrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getAvailabilityFlag() {
        return availabilityFlag;
    }

    public void setAvailabilityFlag(String availabilityFlag) {
        this.availabilityFlag = availabilityFlag;
    }

    public String getAvailabilityQuantity() {
        return availabilityQuantity;
    }

    public void setAvailabilityQuantity(String availabilityQuantity) {
        this.availabilityQuantity = availabilityQuantity;
    }

    public Date getBatch() {
        return batch;
    }

    public void setBatch(Date batch) {
        this.batch = batch;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
