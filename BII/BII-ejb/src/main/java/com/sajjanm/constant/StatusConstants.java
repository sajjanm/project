/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.constant;

/**
 *
 * @author Sajjan Mishra
 */
public enum StatusConstants {
    
    APPROVED("APPROVED"),
    ACTIVE("ACTIVE"),
    PENDING("PENDING"),
    DEACTIVATED("DEACTIVATED"),
    BLOCKED("BLOCKED"),
    DELETE("DELETE"),
    UNBLOCK_PENDING("UNBLOCK_PENDING"),
    UNBLOCK_APPROVE("UNBLOCK_APPROVE"),
    ACCOUNT_VERIFIED("ACCOUNT_VERIFIED"),
    ACCOUNT_DEACTIVATED("ACCOUNT_DEACTIVATED"),
    ACCOUNT_PROFILE_NOT_FOUND("ACCOUNT_PROFILE_NOT_FOUND"),
    ACCOUNT_UNVERIFIED("ACCOUNT_UNVERIFIED");

    private final String name;

    private StatusConstants(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
