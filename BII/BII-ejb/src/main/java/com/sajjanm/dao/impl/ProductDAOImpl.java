package com.sajjanm.dao.impl;

import com.sajjanm.constant.StatusConstants;
import com.sajjanm.dao.ProductDAO;
import com.sajjanm.entity.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sajjan Mishra
 */
@Stateless
public class ProductDAOImpl implements ProductDAO {

    @PersistenceContext(unitName = "BII_PU")
    private EntityManager em;

    @Override
    public boolean save(Product product) {
        try {
            em.persist(product);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean update(Product product) {
        try {
            em.merge(product);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Product getById(Long id) {
        return (Product) em.createQuery("SELECT p from Product p where p.id =:id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long totalCount() {
        return (Long) em.createQuery("SELECT Count(p) FROM Product p WHERE p.status!=:status ").setParameter("status", StatusConstants.DELETE.getName()).getSingleResult();
    }

    @Override
    public List<Product> getAll() {
        return (List<Product>) em.createQuery("SELECT p from Product p WHERE p.status !=:status").setParameter("status", StatusConstants.DELETE.getName()).getResultList();
    }

    @Override
    public Product getBySku(String sku) {
        try {
            return (Product) em.createQuery("SELECT P FROM Product P WHERE P.sku =:sku").setParameter("sku", sku).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Product> manageSearch(String description, String vendorName, String customerPrice) {
        
        return (List<Product>) em.createQuery("SELECT p FROM Product p WHERE p.productDescription LIKE :description OR p.customerPrice =:customerPrice OR p.vendorName LIKE :vendorName")
                .setParameter("description", "%" + description + "%")
                .setParameter("customerPrice", customerPrice)
                .setParameter("vendorName", "%" + vendorName + "%")
                .getResultList();
    }

}
