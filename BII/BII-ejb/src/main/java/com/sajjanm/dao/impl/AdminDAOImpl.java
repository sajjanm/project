/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.dao.impl;

import com.sajjanm.dao.AdminDAO;
import com.sajjanm.entity.Admin;
import com.sajjanm.request.AdminRequest;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Sajjan
 */
@Stateless
public class AdminDAOImpl implements AdminDAO{

    
    @PersistenceContext(unitName = "BII_PU")
    private EntityManager em;
    
    @Override
    public boolean save(Admin t) {
        try {
            em.persist(t);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean update(Admin t) {
        try {
            em.merge(t);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Admin getById(Long id) {
        return (Admin) em.createQuery("SELECT A FROM Admin AS A WHERE A.id =:id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long totalCount() {
        return (Long) em.createQuery("SELECT COUNT(a) FROM Admin AS a").getSingleResult();
    }

    @Override
    public List<Admin> getAll() {
        return (List<Admin>) em.createQuery("select a from Admin AS a").getResultList();
    }

    @Override
    public boolean verifyPassword(AdminRequest admin) {
        Admin tempAdmin = new Admin();
        tempAdmin = (Admin) em.createQuery("SELECT A FROM Admin as A WHERE A.userName =:username").setParameter("username", admin.getUserName()).getSingleResult();
        if(tempAdmin.getPassword().equalsIgnoreCase(admin.getPassword())){
            return true;
        }
        return false;
    }
    
}
