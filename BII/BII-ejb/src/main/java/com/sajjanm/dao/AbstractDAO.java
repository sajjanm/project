package com.sajjanm.dao;

import java.util.List;

/**
 *
 * @author Sajjan Mishra
 */
public interface AbstractDAO<T> {

    public boolean save(T t);

    public boolean update(T t);

    public T getById(Long id);

    public Long totalCount();

    public List<T> getAll();

}
