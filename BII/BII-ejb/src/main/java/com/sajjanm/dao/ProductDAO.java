package com.sajjanm.dao;

import com.sajjanm.entity.Product;
import java.util.List;

/**
 *
 * @author Sajjan Mishra
 */
public interface ProductDAO extends AbstractDAO<Product>{
    
    public Product getBySku(String sku);
    
    public List<Product> manageSearch(String description, String vendorName, String customerPrice);
}