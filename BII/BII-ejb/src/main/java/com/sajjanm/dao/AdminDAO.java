/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm.dao;

import com.sajjanm.entity.Admin;
import com.sajjanm.request.AdminRequest;

/**
 *
 * @author Sajjan
 */
public interface AdminDAO extends AbstractDAO<Admin>{
    
    public boolean verifyPassword(AdminRequest admin);
}
