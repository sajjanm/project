/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sajjanm;

import com.sajjanm.service.ExcelService;
import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * REST Web Service
 *
 * @author Sajjan Mishra
 */
@Path("upload")
@RequestScoped
public class UploadResource {

    @Inject
    private ExcelService excelService;

    /**
     * Retrieves representation of an instance of com.sajjanm.UploadResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        return excelService.test();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadExcel(
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition contentDisposition) throws IOException {

        return excelService.convertFromExcel(inputStream);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/SEARCH")
    public Response ManageSearch(@DefaultValue(value = "") @QueryParam("description") String description,
            @DefaultValue(value = "") @QueryParam("vendorName") String vendorName,
            @DefaultValue(value = "") @QueryParam("price") String customerPrice) {
        System.out.println("the value of description: " + description + " vendorName = " + vendorName + " price was " + customerPrice);

        return excelService.manageSearch(description, vendorName, customerPrice);

//        return Response.ok(excelService.manageSearch(description, vendorName, customerPrice)).build();
    }

    @GET
    @Path("{id}")
    public Response getProductById(@PathParam("id") Long id) {

        return excelService.getProductById(id);

    }

//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Path("/SEARCH/searchProduct")
//    @POST
//    public Response searchProduct(SearchRequest request) {
//        System.out.println("search value is  " + request.getSearchValue());
//        return Response.ok(excelFileDetailService.searchProduct(request.getSearchValue())).build();
//    }
}
