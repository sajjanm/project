import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdminUser } from '../views/login/shared/adminUser';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
  responseType: 'text' as 'json'
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseAdminUrl:string = 'http://localhost:8080/BII-web/api/admin';

  constructor(private http:HttpClient) { }

  // performing login validation
  login(admin: AdminUser):Observable<any> {
    return this.http.post<any>(this.baseAdminUrl, admin, httpOptions); 
    
  }
  
}
