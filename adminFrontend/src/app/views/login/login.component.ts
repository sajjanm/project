import { Component, OnInit , Input} from '@angular/core';
import { AdminUser } from './shared/adminUser';
import { LoginService } from '../../service/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent  implements OnInit{ 

  constructor(private loginService: LoginService, private router: Router){}

  username: string;
  password: string;

  submitted:boolean = false;

  onSubmit(){
    let adminUser: AdminUser = {
      userName : this.username,
      password : this.password
    
    };

    this.loginService.login(adminUser).subscribe(
      
     success =>{
       console.log("success case");
       console.log(success);
      //  if(r.status == 200){
        this.router.navigateByUrl('/dashboard');
      //  }
     },
     error => {
       console.log("error case");
       console.log(error);
     } 
    );
  }

  ngOnInit(){}

}
